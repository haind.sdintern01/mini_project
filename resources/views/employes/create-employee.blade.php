@extends('layouts.body')
@section('index')
<div class="content-wrapper">
    <section class="content" style="margin-left:400px;">
<div class="row">
<div class="col-md-6">
<div class="card card-primary" >
    <div class="card-header">
    <h3 class="card-title" >Thêm thông tin nhân viên</h3>
    </div>
    <form style="" class="row g-3 needs-validation"
            action="{{ route('employee.store') }}" method="POST"
            enctype="multipart/form-data">
            @csrf
    <div class="card-body">
        <div class="form-group">
            <label for="inputName">Tên</label>
            <input type="text" class="form-control" id="validationCustom02"name="ten"  required>
        </div>
        <div class="form-group">
            <label for="inputClientCompany">Email</label>
            <input type="email" id="inputClientCompany" class="form-control" name="mail"required>
        </div>

        <div class="form-group">
            <label for="inputStatus">Chức vụ</label>
            <select id="inputStatus" class="form-control custom-select" name="chucvu" >
                @foreach ($roles as $role)
                <option value="{{ $role->id }}">{{ $role->position }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="inputStatus">Phòng ban</label>
            <select id="inputStatus" class="form-control custom-select" name="phongban" >
                @foreach ($departments as $department)
                <option value="{{ $department->id }}">{{ $department->name }}</option>
                @endforeach
            </select>
        </div>
    <div class="col-12">
                <button class="btn btn-primary" type="submit" >Thêm</button>
            </div>
        </form>
    <!-- /.card-body -->
</div>
<!-- /.card -->
</div>
</div>
</section>
</div>
@endsection


