@extends('layouts.body')
@section('index')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Chỉnh sửa thông tin nhân viên</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">


                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content" style="margin-left:400px;">
            <div class="row">
                <div class="col-md-6">
                <div class="card card-primary" >
                    <form style="" class="row g-3 needs-validation"
                            action="{{ route('employee.update', $employes->id) }}" method="POST"
                            enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <input type="hidden" name="id" value="">
                    <div class="card-body">
                    <div class="form-group">
                        <label for="inputName">Tên</label>
                    <input type="text" class="form-control" id="validationCustom02"name="ten" value="{{ $employes->name }}"  required>
                    </div>
                    <div class="form-group">
                        <label for="inputStatus">Chức vụ</label>
                        <select id="inputStatus" class="form-control custom-select" name="chucvu" required>
                            <option value="{{ $employes->role->id }}">{{ $employes->role->position }}</option>
                            @foreach ($roles as $role)
                            <option value="{{ $role->id }}">{{ $role->position }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="inputStatus">Phòng ban</label>
                        <select id="inputStatus" class="form-control custom-select" name="phongban" required>
                            <option value="{{ $employes->department->id }}">{{ $employes->department->name }}</option>
                            @foreach ($departments as $department)
                            <option value="{{ $department->id }}">{{ $department->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="inputClientCompany">Email</label>
                        <input type="email" id="inputClientCompany" class="form-control" name="mail"value="{{ $employes->email }}">
                    </div>
                    <div class="col-12">
                                <button class="btn btn-primary" type="submit" >Nhấn để sửa</button>
                            </div>
                        </form>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
                </div>


            </div>
            </section>
    <!-- /.content -->
</div>
@endsection
