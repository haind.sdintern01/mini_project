@extends('layouts.body')
@section('index')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Danh sách các phòng ban</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item active">Trang chủ</li>
                        <li class="breadcrumb-item active">Danh sách các phòng ban</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-hover">
                                <thead>
                                    <tr>

                                        <th>ID</th>
                                        <th>Tên phòng ban</th>
                                        <th>Địa chỉ</th>
                                        @if (auth()->user()->level == 0)
                                        <th>Sửa</th>
                                        <th>Xóa</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach( $department as $value)
                                    <tr>
                                        <th scope="row"><?=$value["id"]; ?></th>
                                        <td>{{ $value->name }}</td>
                                        <td>{{ $value->address }}</td>
                                        @if (auth()->user()->level == 0)
                                        <td>
                                            <a href="{{ route('department.edit', $value->id) }}"><button type='submit' class='btn bg-yellow    btn-flat'  name=''><i class='fa fa-edit'></i></button></a>
                                        </td>
                                        <td>
                                            <form action="{{ route('department.destroy', $value->id) }}" method="POST" onclick="return confirm('Are you sure?')">
                                                @csrf
                                                @method('DELETE')
                                                <button type='submit' class='btn bg-red btn-flat' data-toggle='modal' data-target='#exampleModal'><i class='fa fa-trash'></i></button></a>
                                            </form>
                                        </td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection


