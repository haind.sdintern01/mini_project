@extends('layouts.body')
@section('index')
<div class="content-wrapper" style="min-height: 165px;">
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Trang chủ</h1>
				</div><!-- /.col -->

			</div>
		</div>
	</div>
	<section class="content" >
		<div class="container-fluid" style="min-height:165px;" >
			<div class="row" style="weight:265px;">
				<div class="col-lg-3 col-6" >
					<!-- small box -->
					<div class="small-box bg-info"  style="weight:265px;" >
						<div class="inner">
							<h4>PHÒNG BAN</h4>
						</div>
						<div class="icon">
						</div>
						<a href="{{ route('department.index') }}" class="small-box-footer">Chi tiết <i class="fas fa-arrow-circle-right"></i></a>
					</div>
				</div>
				<!-- ./col -->
				<div class="col-lg-3 col-6">
					<!-- small box -->
					<div class="small-box bg-success">
						<div class="inner">
                        <h4>CHỨC VỤ</h4>
						</div>
						<div class="icon">
							<i class="ion ion-stats-bars"></i>
						</div>
						<a href="{{ route('role.index') }}" class="small-box-footer">Chi tiết <i class="fas fa-arrow-circle-right"></i></a>
					</div>
				</div>
				<!-- ./col -->
				<div class="col-lg-3 col-6">
					<!-- small box -->
					<div class="small-box bg-warning">
						<div class="inner">
							<h4 style="color: #fff">NHÂN VIÊN</h4>
						</div>
						<div class="icon">
							<i class="ion ion-person-add"></i>
						</div>
						<a href="{{ route('employee.index') }}" class="small-box-footer">Chi tiết <i class="fas fa-arrow-circle-right"></i></a>
					</div>
				</div>
				<!-- ./col -->
			</div>
		</div>
	</section>
</div>
@endsection
