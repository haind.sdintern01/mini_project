<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;
    protected $table = 'departments';
    protected $primarykey = 'id';
    public $timestamps = true;
    protected $fillable = ['name', 'address'];
    public function employee()
    {
        return $this->hasMany(Employee::class, 'department_id', 'id');
    }
}
