<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;
    protected $table = 'roles';
    protected $primarykey = 'id';
    public $timestamps = true;
    protected $fillable = ['position'];
    public function employee()
    {
        return $this->hasMany(Employee::class, 'role_id', 'id');
    }
}
