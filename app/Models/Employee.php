<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;
    protected $table = 'users';
    protected $primarykey = 'id';
    public $timestamps = true;
    protected $fillable = ['name', 'role_id', 'department_id', 'email', 'username', 'password'];
    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id', 'id');
    }
    public function department()
    {
        return $this->belongsTo(Department::class, 'department_id', 'id');
    }

}
