<?php
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::group(function () {
    Route::middleware('user')->group(function () {
        Route::resource('department', DepartmentController::class);
        Route::resource('role', RoleController::class);
        Route::resource('employee', EmployeeController::class);
     });
// })
// ->middleware(['isAdmin' excep()]);
// Route::get('/department', [DepartmentController::class, 'index'])->name('home');
// Route::get('/department/create', [DepartmentController::class, 'create'])->name('home');
// Route::post('/department', [DepartmentController::class, 'store'])->name('home');
// Route::get('/department/', [HomeController::class, 'index'])->name('home');
// Route::get('/index', [HomeController::class, 'index'])->name('home');



Route::middleware(['user'])->group(function () {

});

//         Route::resource('department', DepartmentController::class);
//         Route::resource('role', RoleController::class);
//         Route::resource('employee', EmployeeController::class);

Route::get('/', function () {
    return view('index');
})->middleware(['user'])->name('dashboard');
require __DIR__.'/auth.php';
Route::post('login','LoginController@check');
Auth::routes();
Route::get('/index', [HomeController::class, 'index'])->name('home');
Route::controller(UserController::class)->group(function(){
    Route::get('users', 'index');
    Route::get('users-export', 'export')->name('users.export');
});

